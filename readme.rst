typewriter.vim
==============

makes clicky noises when you insert characters and carriage returns, and warn
a few characters before wrapping with a bell

usage
-----

1. install with your favorite vim plugin manager
2. activate with :code:`:TypewriterOn`

best enjoyed with `Goyo`_

.. _`Goyo`: https://github.com/junegunn/goyo.vim

configuration
-------------

Use :code:`g:typewriter_player` to change the executable used to play sounds
(defaults to :code:`afplay` on macOS and :code:`paplay` otherwise). For example
to play with ALSA_:

.. code:: bash

    let g:typewriter_player="aplay"

.. _ALSA: https://wiki.archlinux.org/title/Advanced_Linux_Sound_Architecture
