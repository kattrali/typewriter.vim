if !exists("g:typewriter_on")
    let g:typewriter_on=1
endif

if !exists("g:typewriter_player")
    if has('macunix')
        let g:typewriter_player="afplay"
    else
        let g:typewriter_player="paplay"
    endif
endif

let s:plugindir = expand('<sfile>:p:h:h')
let s:belloffset = 8

function s:getResource(name)
    return s:plugindir . '/res/' . a:name
endfunction

function s:playSound(name)
    let l:cmd = g:typewriter_player . " " . s:getResource(a:name)
    if has('nvim')
        call jobstart(cmd)
    else
        call job_start(cmd)
    endif
endfunction

function! typewriter#DidInsert()
    if g:typewriter_on
        if &textwidth == col(".") + s:belloffset
            call s:playSound("typewriter-bell.wav")
        else
            let l:type = (match(v:char, '\S') == -1 ? "soft" : "click")
            call s:playSound("typewriter-" . l:type . ".wav")
        endif
    endif
endfunction

function! typewriter#DidReturn()
    if g:typewriter_on
        call s:playSound("typewriter-return.wav")
    endif
endfunction

function! typewriter#TypewriterOn()
    let g:typewriter_on=1
endfunction

function! typewriter#TypewriterOff()
    let g:typewriter_on=0
endfunction

function! typewriter#TypewriterToggle()
    let g:typewriter_on=(g:typewriter_on == 0 ? 1 : 0)
endfunction

autocmd InsertCharPre * call typewriter#DidInsert()
inoremap <CR> <CR><C-O>:call typewriter#DidReturn()<CR>
