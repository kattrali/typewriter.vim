command! -bang -nargs=0 TypewriterOn call typewriter#TypewriterOn()
command! -bang -nargs=0 TypewriterOff call typewriter#TypewriterOff()
command! -bang -nargs=0 TypewriterToggle call typewriter#TypewriterToggle()
